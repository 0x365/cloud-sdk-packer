FROM gcr.io/google.com/cloudsdktool/cloud-sdk:323.0.0-alpine
WORKDIR /app

ENV PACKER_VERSION 1.6.6

RUN set -e \
    && apk add --no-cache \
        ansible \
        unzip \ 
        wget \
    && rm -f /var/cache/apk/*

RUN wget https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip \
    && unzip packer_${PACKER_VERSION}_linux_amd64.zip \
    && rm packer_${PACKER_VERSION}_linux_amd64.zip \
    && mv packer /usr/bin