# cloud-sdk-packer
This image contains a customized version of packer used to build our OS images in GCP

## How to use this image

Print help:

```bash
docker run --rm us.gcr.io/x365-pipes/tools/packer --help
```

## Docker image

us.gcr.io/x365-pipes/tools/packer

## Tools included

- packer
- ansible
- unzip

## Variables

| Variable                   | Usage                                                |
| ----------------------------- | ---------------------------------------------------- |

_(*) = required variable._